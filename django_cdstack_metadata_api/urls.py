from django.urls import path, include
from tastypie.api import Api

from django_cdstack_metadata_api.django_cdstack_metadata_api.api.resources import *

v1_api = Api(api_name="v1")
v1_api.register(PublicResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
