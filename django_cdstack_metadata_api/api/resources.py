from tastypie.authentication import Authentication, ApiKeyAuthentication
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_cdstack_models.django_cdstack_models.models import CmdbMetaData


class MetaDataBaseResource(Resource):
    class Meta:
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiKeyAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return


class PublicResource(MetaDataBaseResource):
    class Meta(MetaDataBaseResource.Meta):
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = Authentication()

    def obj_get(self, bundle, **kwargs):
        metadata_id = int(kwargs["pk"])

        bundle = CmdbMetaData.objects.get(id=metadata_id)

        return bundle

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = dict()

            for meta_row in bundle.obj.cmdbvarsmetadata_set.all():
                bundle.data[meta_row.name] = meta_row.value

        return bundle
